import collections
import json
import logging
import random
import string
import time

import server


TASK_SIZE = 20


class Client(server.Server):
    """
    Sends tasks to dispatcher to calculate.
    Keeps statistics about sent tasks.
    Statistics output - logging.info
    """

    def __init__(self,
                 addr, dispatcher,
                 min_gen_time, max_gen_time,
                 task_timeout,
                 clean_up_timeout=None,
                 log_timeout=None,
                 ):
        """
        :param addr: tuple (url/ip, port)
        :param dispatcher: tuple (url/ip, port)
        :param min_gen_time: int, ms. Min time between generating tasks
        :param max_gen_time: int, ms. Max time between generating tasks
        :param task_timeout: int, ms. Max time for task to be completed
        :param clean_up_timeout: int, ms. Delay between checks all tasks to find incomplete ones
        :param log_timeout: int, ms. Delay between log messages
        """
        regular_tasks = [
            (self._send_task, min_gen_time, max_gen_time),
        ]
        if clean_up_timeout:
            regular_tasks.append((self._cleanup_timeout, clean_up_timeout, clean_up_timeout))
        if log_timeout:
            regular_tasks.append((self._log, log_timeout, log_timeout))
        super(Client, self).__init__(addr, regular_tasks)

        self._dispatcher = dispatcher
        self._addr = addr
        self._active_tasks = {}
        self._timeout = float(task_timeout) / 1000

        self._sent = 0
        self._complete = 0
        self._incomplete = 0
        self._answer_times = []

    def _send_task(self):
        task = ''.join(random.choice(string.ascii_letters) for _ in range(TASK_SIZE))
        self._active_tasks[task] = time.time()
        msg = json.dumps(dict(
            task=task,
            port_to_answer=self._addr[1],
            sender='client'
        ))
        self._send(msg=msg, full_addr=self._dispatcher)
        self._sent += 1
        logging.debug('sent msg: {0}'.format(msg))

    def _cleanup_timeout(self):
        cur_time = time.time()
        incomplete_tasks = [
            task for task, start_time in self._active_tasks.items()
            if cur_time-start_time > self._timeout
        ]
        for task in incomplete_tasks:
            del self._active_tasks[task]
        self._incomplete += len(incomplete_tasks)

    def _log(self):
        field_values = sorted(
            self._get_statistics().items(),
            key=lambda x:('time' in x[0], x[0])
        )
        field_values = (
            (k, '{0:.2f}'.format(v) if isinstance(v, float) else v)
            for k, v in field_values
        )
        log_msg = '  '.join('{0}:{1:>5}'.format(k, v) for k, v in field_values)
        print log_msg

    def _on_listen(self, addr_name, msg):
        task = msg.get('task')
        if not task:
            logging.warning('wrong message received: {0}'.format(msg))
            return
        start_time = self._active_tasks.pop(task, None)
        if not start_time:
            return
        answer_time = time.time() - start_time
        if answer_time > self._timeout:
            self._incomplete += 1
        else:
            self._complete += 1
            self._answer_times.append(answer_time)

    def _get_statistics(self):
        state = dict(
            sent=self._sent,
            complete=self._complete,
            incomplete=self._incomplete,
        )
        if self._answer_times:
            time_info = dict(
                max_answer_time=max(self._answer_times),
                min_answer_time=min(self._answer_times),
                middle_answer_time=sum(self._answer_times)/len(self._answer_times),
            )
            state.update(time_info)
        return state


def main():
    with open('config.json', 'r') as fh:
        config = json.load(fh)
    try:
        settings = dict(
            addr=config['listen'],
            dispatcher=config['dispatcher'],
            max_gen_time=config['max_time'],
            min_gen_time=config['min_time'],
            task_timeout=config['task_timeout'],
            clean_up_timeout=config.get('clean_up_timeout'),
            log_timeout=config.get('log_timeout'),
        )
    except KeyError:
        logging.error('Wrong config')
        return
    logging.basicConfig(
        level=logging.DEBUG if config.get('debug') else logging.INFO,
        format='%(levelname)s:%(asctime)s:%(message)s',
    )
    Client(**settings).serve()


if __name__ == '__main__':
    main()
