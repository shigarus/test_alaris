import abc
import json
import logging
import random
import time
import socket


MAX_PACKAGE_SIZE = 1024


class Server:
    """
    Async server.
    Serves timed tasks - regular or one-time.
    Transport protocol - UDP.
    Message protocol - json, can contains only MAX_PACKAGE_SIZE symbols.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, addr, regular_tasks=()):
        """
        :param addr: tuple with two values:
            1) target ip or url
            2) target port
        :param regular_tasks: tuple. Each item consists of 3 parameters:
            1) function to call
            2) min time to recall
            3) max time to recall
        """
        self.__listener = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__listener.bind(tuple(addr))
        self.__listener.setblocking(0)
        self.__writer = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__regular_tasks_settings = {}
        self.__do_task_at = {}
        for task, min_time, max_time in regular_tasks:
            self.add_regular_task(task, min_time, max_time)

    def __do_tasks(self):
        cur_time = time.time()
        tasks_to_do = [
            task
            for task, time_to_do in self.__do_task_at.items()
            if time_to_do < cur_time
        ]

        for task in tasks_to_do:
            task()
            if task in self.__regular_tasks_settings:
                self.__renew_regular_task(task)
            else:
                del self.__do_task_at[task]

    def __renew_regular_task(self, task):
        pulse_time = random.randint(*self.__regular_tasks_settings[task])
        self.__do_task_at[task] = time.time() + float(pulse_time) / 1000

    def __on_listen(self, addr_name, msg):
        try:
            msg = json.loads(msg)
            if not isinstance(msg, dict):
                raise ValueError
        except ValueError:
            logging.warning('wrong message received: {0}'.format(msg))
            return
        logging.debug('received: {0}'.format(msg))
        self._on_listen(addr_name, msg)

    @abc.abstractmethod
    def _on_listen(self, addr_name, msg):
        """
        called on every received message
        :param addr_name: str, sender of the message
        :param msg: dict
        """
        raise NotImplementedError

    def _send(self, addr_name=None, port=None, msg=None, full_addr=None):
        """
        sends json-msg over UDP
        :param addr_name: str, target url or ip.
        :param port: str or int, target port
        :param msg: str
        :param full_addr: tuple (addr_name, port)
        """
        if not (addr_name and port):
            addr_name, port = full_addr
        if isinstance(msg, dict):
            msg = json.dumps(msg)
        if len(msg) > MAX_PACKAGE_SIZE:
            raise ValueError('data is too large, maximum is {0}'.format(MAX_PACKAGE_SIZE))
        while True:
            try:
                self.__writer.sendto(msg.ljust(MAX_PACKAGE_SIZE), (addr_name, port))
                break
            except socket.error:
                pass

    def add_delayed_task(self, task, delay):
        """
        task will be called after delay
        :param task: function
        :param delay: time, ms
        """
        self.__do_task_at[task] = time.time() + float(delay) / 1000

    def add_regular_task(self, task, min_time, max_time):
        """
        task will be called on regular basis, every second call may be between min_time and max_time
        min_time and max_time can be equal
        :param task: function
        :param min_time: int, ms
        :param max_time: int, ms
        """
        self.__regular_tasks_settings[task] = [min_time, max_time]
        self.__renew_regular_task(task)

    def serve(self):
        """
        run event loop forever
        """
        while True:
            time.sleep(0.001)  # hack to reduce cpu load. enough for test event loop
            try:
                data, (addr, _) = self.__listener.recvfrom(1024)
                self.__on_listen(addr, data)
            except socket.error:
                pass
            self.__do_tasks()


class _TestServer(Server):

    def __init__(self, port_to_send, start_task, *args, **kwargs):
        super(_TestServer, self).__init__(*args, **kwargs)
        self.port_to_send = port_to_send
        if start_task:
            addr_name, msg = start_task
            self.add_delayed_task(lambda: self._send(addr_name, port_to_send, msg), 0)

    def _on_listen(self, addr_name, msg):
        logging.debug('received {0} from {1}'.format(msg, addr_name))
        self._send(addr_name, self.port_to_send, msg)

    def _send(self, *args, **kwargs):
        res = super(_TestServer, self)._send(*args, **kwargs)
        logging.debug('called self._send with {0} {1}'.format(args, kwargs))
        return res


def test():
    logging.basicConfig(level=logging.DEBUG)

    def regular_task():
        text = 'asd' * 100
        logging.debug('{0}: {1}'.format(time.time(), text))
    _TestServer(
        port_to_send=6002,
        start_task='',
        addr=('', 6001),
        regular_tasks=((regular_task, 1000, 1000), ),
    ).serve()


if __name__ == '__main__':
    test()
