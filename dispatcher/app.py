import collections
import json
import logging
import time

import server


class Dispatcher(server.Server):
    """
    Receives tasks from clients and redirects them to calculators.
    Notifies clients about completed tasks.
    Knows about every connected calculator.
    """

    def __init__(self, addr, task_timeout, calculator_timeout, task_try_timeout):
        """
        :param addr: tuple (url/ip, port)
        :param task_timeout: int, ms. Max time to serve one task
        :param calculator_timeout: int, ms.
            If calculator won't notify about availability - it will be marked as unavailable
        :param task_try_timeout: int, ms. Max time for calculator to serve a task
        """
        super(Dispatcher, self).__init__(addr)

        self._calculators_last_alive = {}
        self._calculators_busy = {}

        self._task_received = {}
        self._task_to_client = {}
        self._tasks_started_at = {}
        self._waiting_tasks = collections.deque()

        self._task_timeout = task_timeout
        self._calculator_timeout = calculator_timeout
        self._task_try_timeout = task_try_timeout

    @staticmethod
    def _check_msg_schema(msg):
        schemas = [
            # from client
            ['sender', 'task', 'port_to_answer'],
            # from calculator
            ['sender', 'type', 'port_to_answer', 'has_task'],
            ['sender', 'type', 'port_to_answer', 'task'],
        ]
        is_valid = any(
            all(field in msg for field in sch)
            for sch in schemas
        )
        if not is_valid:
            logging.warning('wrong msg schema: {0}'.format(msg))
            return False

        port = msg['port_to_answer']
        try:
            port = int(port)
            if port < 1024 or port > 65534:
                raise ValueError
        except ValueError:
            logging.warning('Invalid port: {0}'.format(port))
            return False

        if msg['sender'] not in ('client', 'calculator'):
            logging.warning('wrong sender type: {0}'.format(msg['sender']))
            return False

        return True

    def _on_listen(self, addr_name, msg):
        if not self._check_msg_schema(msg):
            return
        sender_type = msg.get('sender')
        if sender_type == 'client':
            self._serve_client(addr_name, msg)
        elif sender_type == 'calculator':
            self._serve_calculator(addr_name, msg)

    def _serve_client(self, addr_name, msg):
        task, port = msg['task'], msg['port_to_answer']
        self._task_received[task] = time.time()
        self._task_to_client[task] = (addr_name, int(port))
        self._waiting_tasks.append(task)
        self._send_tasks()

    def _serve_calculator(self, addr_name, msg):
        msg_type = msg.get('type')
        if msg_type == 'alive_notify':
            self._update_alive_calculator(addr_name, msg)
        elif msg_type == 'task_complete':
            self._update_alive_calculator(addr_name, msg)
            self._complete_task(msg['task'])
        else:
            logging.warning('wrong msg type: {0}'.format(msg_type))

    def _update_alive_calculator(self, addr_name, msg):
        calculator = (addr_name, int(msg['port_to_answer']))
        self._calculators_last_alive[calculator] = time.time()
        has_task = msg.get('has_task', False)
        if not has_task:
            self._calculators_busy.pop(calculator, None)

    def _send_tasks(self):
        calculator = self._get_free_calculator()
        while calculator and self._waiting_tasks:
            task = self._waiting_tasks.popleft()
            if not self._is_actual_task(task):
                logging.info('removing non-actual task {0}'.format(task))
                continue
            logging.info('sending task {0} to {1}'.format(task, calculator))
            self._send(full_addr=calculator, msg=dict(task=task))
            self._tasks_started_at[task] = time.time()
            self._calculators_busy[calculator] = task
            self.add_delayed_task(
                lambda: self._redirect_task(task),
                self._task_try_timeout,
            )
            calculator = self._get_free_calculator()

    def _is_actual_task(self, task):
        start_time = self._task_received.get(task)
        if not start_time:
            return False
        working = time.time() - start_time
        return working < float(self._task_timeout) / 1000

    def _redirect_task(self, task):
        if self._is_actual_task(task):
            # there is time to try another calculator
            self._waiting_tasks.append(task)
            self._send_tasks()
        else:
            # this task is served too long
            self._clear_task_info(task)

    def _clear_task_info(self, task):
        self._task_received.pop(task, None)
        self._task_to_client.pop(task, None)
        self._tasks_started_at.pop(task, None)

    def _get_free_calculator(self):
        for calculator, last_alive in self._calculators_last_alive.items():
            not_busy = calculator not in self._calculators_busy
            alive = time.time() - last_alive < self._calculator_timeout
            if not_busy and alive:
                return calculator
        logging.info('wait free calculator')

    def _complete_task(self, task):
        start_time = self._task_received.get(task, None)
        if start_time and task in self._task_to_client:
            self._send(
                full_addr=self._task_to_client[task],
                msg=dict(task=task),
            )
            complete_time = time.time() - start_time if start_time else 'UNKNOWN'
            logging.info('informing about task {0} is complete in {1:.2f} sec'.format(task, complete_time))
        self._clear_task_info(task)


def main():
    with open('config.json', 'r') as fh:
        config = json.load(fh)
    try:
        settings = dict(
            addr=config['listen'],
            task_timeout=config['task_timeout'],
            calculator_timeout=config['calculator_timeout'],
            task_try_timeout=config['task_try_timeout'],
        )
    except KeyError:
        logging.error('Wrong config')
        return
    logging.basicConfig(
        level=logging.DEBUG if config.get('debug') else logging.INFO,
        format='%(levelname)s:%(asctime)s:%(message)s',
    )
    Dispatcher(**settings).serve()


if __name__ == '__main__':
    main()
