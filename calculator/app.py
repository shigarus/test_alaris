import json
import logging
import random

import server


class Calculator(server.Server):
    """
    Emulates calculating received tasks.
    Can be broken randomly, but still process calculating task.
    Notifies dispatcher about availability of itself.
    """

    def __init__(self, addr, dispatcher, time_to_calculate, alive_timeout,
                 check_break_timeout, break_timeout, break_chance,
                 ):
        """
        :param addr: tuple (url/ip, port)
        :param dispatcher: tuple (url/ip, port)
        :param time_to_calculate: int, ms. time to calculate single task
        :param alive_timeout: int, ms. Calculator notifies dispatcher about availability of itself every alive_timeout
        :param check_break_timeout: int, ms. Can be broken every check_break_timeout
        :param break_timeout: int, ms. Time of unavailability
        :param break_chance: int, from 0 to 99. Chance to be broken
        """
        regular_tasks = (
            (self._health_notify, alive_timeout, alive_timeout),
            (self._break_service, check_break_timeout, check_break_timeout),
        )
        super(Calculator, self).__init__(addr=addr, regular_tasks=regular_tasks)

        self._port = addr[1]
        self._dispatcher = dispatcher
        self._time_to_calculate = time_to_calculate
        self._alive_timeout = alive_timeout
        self._break_timeout = break_timeout
        self._break_chance = break_chance

        self._cur_task = None
        self._has_complete_task = False
        self._is_alive = True

    def _send_to_dispatcher(self, msg):
        full_msg = dict(
            sender='calculator',
            port_to_answer=self._port,
        )
        full_msg.update(msg)
        self._send(msg=full_msg, full_addr=self._dispatcher)

    def _health_notify(self):
        if not self._is_alive:
            logging.info('skipping')
            return
        msg = dict(type='alive_notify', has_task=bool(self._cur_task))
        self._send_to_dispatcher(msg)

    def _on_listen(self, addr_name, msg):
        task = msg.get('task')
        if not task:
            logging.warning('received msg without task {0}'.format(msg))
            return
        self._start_task(task)

    def _start_task(self, task):
        if self._cur_task:
            logging.info('received task {0} while processing another task'.format(task))
            return
        self._cur_task = task
        time_to_calc = random.randint(*self._time_to_calculate)
        logging.info('received task {0}, processing time {1}'.format(task, time_to_calc))
        self.add_delayed_task(self._complete_task, time_to_calc)

    def _complete_task(self):
        logging.info('completed task {0}'.format(self._cur_task))
        if self._is_alive:
            logging.info('sending complete state to dispatcher')
            msg = dict(type='task_complete', task=self._cur_task)
            self._send_to_dispatcher(msg)
            self._cur_task = None
            self._has_complete_task = False
        else:
            logging.info('server is broken, waiting until it will be restored')
            self._has_complete_task = True

    def _break_service(self):
        if not self._is_alive:
            # do not extend break period
            return
        is_alive = random.randint(0, 100) > self._break_chance
        if is_alive:
            return
        self._is_alive = False
        self.add_delayed_task(self._restore_service, self._break_timeout)
        logging.info('server will be broken for {0} seconds'.format(self._break_timeout/1000))

    def _restore_service(self):
        self._is_alive = True
        logging.info('server restored')
        if self._has_complete_task:
            self._complete_task()


def main():
    with open('config.json', 'r') as fh:
        config = json.load(fh)
    try:
        settings = dict(
            addr=config['listen'],
            dispatcher=config['dispatcher'],
            time_to_calculate=config['time_to_calculate'],
            alive_timeout=config['alive_timeout'],
            check_break_timeout=config['check_break_timeout'],
            break_timeout=config['break_timeout'],
            break_chance=config['break_chance'],
        )
    except KeyError:
        logging.error('Wrong config')
        return
    logging.basicConfig(
        level=logging.DEBUG if config.get('debug') else logging.INFO,
        format='%(levelname)s:%(asctime)s:%(message)s',
    )
    Calculator(**settings).serve()


if __name__ == '__main__':
    main()
